import { apiKey, apiToken } from "../secretFile.js";

export async function getCards(listId) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/lists/${listId}/cards?fields=name,url&key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
