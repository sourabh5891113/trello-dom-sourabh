import { apiKey, apiToken } from "../secretFile.js";

export async function createCard(listId,cardName) {
    let response = await fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${apiKey}&token=${apiToken}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json'
        }
      });
      let data = response.json();
      return data;
  }