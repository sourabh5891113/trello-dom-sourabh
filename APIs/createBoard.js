import { apiKey, apiToken } from "../secretFile.js";

let colorArr = [
  "blue",
  "orange",
  "green",
  "red",
  "purple",
  "pink",
  "lime",
  "sky",
  "grey",
];

export async function createBoard(boardName) {
  let ind = Math.floor(Math.random() * 10);
  let color = colorArr[ind];
  try {
    let response = await fetch(
      `https://api.trello.com/1/boards/?prefs_background=${color}&name=${boardName}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
      }
    );
    let data = response.json();
    console.log(data);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
// export function createBoard(boardName) {
//   fetch(
//     `https://api.trello.com/1/boards/?prefs_background=${color}&name=${boardName}&key=${apiKey}&token=${apiToken}`,
//     {
//       method: "POST",
//     }
//   )
//   .then(response => {
//     console.log(
//       `Response: ${response.status} ${response.statusText}`
//     );
//     return response.text();
//   })
//   .then(text => console.log(text))
//   .catch(err => console.error(err));
// }
