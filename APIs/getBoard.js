import { apiKey, apiToken } from "../secretFile.js";

export async function getBoard(boardId) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/boards/${boardId}?fields=name,url,prefs&key=${apiKey}&token=${apiToken}`
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
