import { apiKey, apiToken } from "../secretFile.js";

export function deleteBoard(boardId) {
  fetch(`https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}`, {
    method: "DELETE",
  })
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((text) => console.log(text))
    .catch((err) => console.error(err));
}
