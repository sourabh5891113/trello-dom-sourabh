import { apiKey, apiToken } from "../secretFile.js";

export async function createList(listName, boardId) {
    let response = await fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
      }
    );
    let data = response.json();
    return data;
}