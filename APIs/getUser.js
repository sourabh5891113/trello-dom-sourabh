import {apiKey, apiToken} from "../secretFile.js"
import { getBoard } from "./getBoard.js";


export function getUser(userId) {
   return fetch(
    `https://api.trello.com/1/members/${userId}?fields=fullName,url,username,idBoards&key=${apiKey}&token=${apiToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  ).then((res)=>{
    return res.json();
  }).then((data)=>{
    let boardIds = data.idBoards;
    return Promise.all(boardIds.map((id)=>getBoard(id)));
  })
}


