import { apiKey, apiToken } from "../secretFile.js";

export async function archiveList(listId) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`,
      {
        method: "PUT",
      }
    );
    let data = response.json();
    console.log(data);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
