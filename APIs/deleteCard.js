import { apiKey, apiToken } from "../secretFile.js";

export async function deleteCard(cardId) {
  try {
    let response = await fetch(
      `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`,
      {
        method: "DELETE",
      }
    );
    let data = response.json();
    // console.log(data);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}