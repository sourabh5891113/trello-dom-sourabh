import { getMyBoards } from "./APIs/getMyBoards.js";
import { createBoard } from "./APIs/createBoard.js";

//get all boeards for a user:-
let userId = "sourabhthakur11";

//Recently viwed
let recentlyViewed;
if (sessionStorage.getItem("rv")) {
  recentlyViewed = JSON.parse(sessionStorage.getItem("rv"));
} else {
  recentlyViewed = [];
}
window.addEventListener("DOMContentLoaded", () => {
  //creating recently viewed boards
  let recentBoards = document.getElementById("recently-viewed");
  recentlyViewed.forEach((board) => {
    if (board.backgroundImage) {
      createBoardUI(board, recentBoards, board.backgroundImage);
    } else {
      createBoardUI(board, recentBoards, null, board.backgroundColor);
    }
  });
  getData();
});

//getting boards and creating the UI
async function getData() {
  try {
    const boards = await getMyBoards();
    let cardContainer = document.getElementById("card-container");
    // console.log(boards);
    boards.forEach((board) => {
      if (board.prefs.backgroundImageScaled !== null) {
        let backgroundImage = board.prefs.backgroundImageScaled[0].url;
        createBoardUI(board, cardContainer, backgroundImage);
      } else {
        let backgroundColor = board.prefs.backgroundColor;
        createBoardUI(board, cardContainer, null, backgroundColor);
      }
    });
    addCreateBoardDiv(cardContainer);

    //adding eventLitenners
    let cards = cardContainer.children;
    for (let index = 0; index < cards.length; index++) {
      const element = cards[index];
      if (index === cards.length - 1) {
        //for creating the card
        let createDiv = element.children[0];
        let createModelDiv = element.children[1];
        createDiv.addEventListener("click", () => {
          if (createModelDiv.style.display === "flex") {
            createModelDiv.style.display = "none";
          } else {
            createModelDiv.style.display = "flex";
          }
        });
        let closeBtn = element.children[1].children[1];
        closeBtn.addEventListener("click", () => {
          createModelDiv.style.display = "none";
        });
        let formElement = element.children[1];
        let inputBoardTitle = element.children[1].children[3];
        formElement.addEventListener("submit",(e)=>{
          e.preventDefault();
          let boardName = inputBoardTitle.value;
          if (boardName === "") return;
          createBoard(boardName);
          setTimeout(()=>{
            location.reload();
          },500)
        })
      } else {
        //adding cards too recently viwed
        let id = element.children[0].href.split("=")[1];
        let boardName = element.children[0].children[0].innerText;
        let backgroundColor = element.style.backgroundColor;
        let backgroundImage = element.style.backgroundImage;
        backgroundImage = backgroundImage.replace(/(url\(|\)|")/g, "");
        element.addEventListener("click", () => {
          let boardVisited = {};
          if (backgroundImage) {
            boardVisited = {
              id: id,
              name: boardName,
              backgroundImage: backgroundImage,
            };
          } else {
            boardVisited = {
              id: id,
              name: boardName,
              backgroundColor: backgroundColor,
            };
          }
          let board = recentlyViewed.find((board)=> board.id === id);
          if(!board){
            recentlyViewed.push(boardVisited);
            sessionStorage.setItem("rv", JSON.stringify(recentlyViewed));
          }
        });
      }
    }
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

//function for creting a board card
function createBoardUI(board, cardContainer, backgroundImage, backgroundColor) {
  if (backgroundImage) {
    cardContainer.innerHTML += ` <div class="card w-52 h-32 rounded text-white font-bold" style="background-image: url(${backgroundImage});
        background-size: cover;">
      <a href="board.html?boardId=${board.id}" class="block h-full p-2">
        <h4>${board.name}</h4>
      </a>
    </div>`;
  } else {
    cardContainer.innerHTML += ` <div class="card w-52 h-32 rounded text-white font-bold" style= "background-color: ${backgroundColor}">
      <a href="board.html?boardId=${board.id}" class="block h-full p-2">
        <h4>${board.name}</h4>
      </a>
    </div>`;
  }
}

//function for creting create board div
function addCreateBoardDiv(cardContainer) {
  cardContainer.innerHTML += `<div class="flex relative gap-4">
    <div
      class="card w-52 h-32 rounded flex justify-center items-center cursor-pointer"
      style="background-color: rgba(9, 30, 66, 0.14)"
      id="create-new-board"
    >
      <h4>Create new Board</h4>
    </div>
    <form
      class="hidden flex-col justify-center items-center  p-1 gap-2"
      style="background-color: rgba(9, 30, 66, 0.14)"
      id="create-new-board-model"
    >
      <h4>Create board</h4>
      <span class="absolute top-0 right-3 text-2xl cursor-pointer">x</span>
      <label for="Board title" class="text-sm self-start"
        >Board title</label
      >
      <input type="text" required class="border border-black px-1 rounded" />
      <input type="submit" class="text-white bg-blue-700 hover:bg-blue-800 font-medium rounded text-sm px-5 py-1  focus:outline-none w-full" id="create-button" value="Create">
    </form>
  </div>`;  
}
