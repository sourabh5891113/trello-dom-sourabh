import { getMyBoards } from "./APIs/getMyBoards.js";
import { getBoard } from "./APIs/getBoard.js";
import { getLists } from "./APIs/getLists.js";
import { getCards } from "./APIs/getCards.js";
import { createList } from "./APIs/createList.js";
import { createCard } from "./APIs/createCard.js";
import { archiveList } from "./APIs/archiveList.js";
import { deleteCard } from "./APIs/deleteCard.js";
import { deleteBoard } from "./APIs/deleteBoard.js";

let boardId = window.location.href.split("?")[1].split("=")[1];

//geting boards title
async function getTitle(boardId){
    let board = await getBoard(boardId);
    const boardTitle = document.getElementById("board-title");
    boardTitle.innerText = board.name;
}
getTitle(boardId);

//getting boards and putting on sidebar
async function getAllBoards() {
  const boards = await getMyBoards();
  const parentLi = document.getElementById("all-boards");
  parentLi.innerHTML = ` <h1 class="font-bold">Your boards</h1>`;
  boards.forEach((board) => {
    createBoardDiv(board, parentLi);
  });
  parentLi.addEventListener('click',(e)=>{
    if(e.target.className === "board-tile"){
      window.location.href =  window.location.origin+"/board.html?boardId="+ e.target.dataset.id;
    }
  })
  deleteBoardUI(boards);
}
getAllBoards();

//adding eventListener for deleting board
function deleteBoardUI(boards){
  const dl = document.getElementsByClassName('board-tile');
  for (let index = 0; index < dl.length; index++) {
    const element = dl[index];
    const board = boards[index];
    let li = element.children[2].children[1].children[0];
    li.addEventListener("click",()=>{
      deleteBoard(board.id);
      sessionStorage.clear();
      setTimeout(()=>{
        window.location.href =  window.location.origin+"/index.html"
      },1000)
    })
  }
}

//getting board's lists add adding it to the DOM
async function getAllLists() {
  const lists = await getLists(boardId);
  const listCont = document.getElementById("list-container");
  lists.forEach((list) => {
    createListAndCards(list, listCont);
  });
  listCont.innerHTML += ` <div class="h-12 flex items-center gap-3 rounded-lg pl-4 w-80 bg-blue-300 text-white" id="add-list">
    <div class="flex gap-3 pointer-events-none">
      <p>+</p>
      <p>Add another list</p>
    </div>
        <div class="hidden flex-wrap gap-3">
          <input
            type="text"
            placeholder="Enter list title..."
            class="input w-11/12 bg-white text-black"
          />
          <div class="flex gap-3">
            <button class="btn btn-primary">Add list</button>
            <button class="btn btn-square" id="close-btn">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /></svg>
            </button>
          </div>
        </div>  
    </div>`;

  renderCards(lists);
  createListEL();
  createCardEL(lists);
  archiveListUI(lists);
}
getAllLists();

//function for adding cards to the Lists
async function renderCards(lists) {
  const cardContainers = document.getElementsByClassName("card-conatiner");
  let allCards = [];
  for (let index = 0; index < cardContainers.length; index++) {
    const element = cardContainers[index];
    const cards = await getAllCards(lists[index].id);
    cards.forEach((card) => {
      allCards.push(card);
      element.innerHTML += `<div class="h-10 bg-white rounded-lg shadow-md py-2 px-3 relative peer" data-id="${card.id}">
            ${card.name}
            <div class="hidden absolute w-3 top-3 right-4 cursor-pointer delete-card-btn">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>
            </div>
          </div>`;
        });
      }
      deleteCardUI(allCards)
}
//attaching eventlisteners for deleting a card 
function deleteCardUI(allCards){
  const deleteCardBtns = document.getElementsByClassName("delete-card-btn");

  for (let index = 0; index < deleteCardBtns.length; index++) {
    const element = deleteCardBtns[index];
    const cardId =  allCards[index].id;
    element.addEventListener('click',()=>{
      deleteCard(cardId);
      setTimeout(()=>{
        location.reload();
      },500)
    })
  }
}
async function getAllCards(id) {
  let cards = await getCards(id);
  return cards;
}

//creating boards for sidebar
function createBoardDiv(board, parentLi) {
  const backgroundColor = board.prefs.backgroundColor;
  parentLi.innerHTML += `<div data-id="${board.id}" class="board-tile">
        <div class="w-5 h-4 rounded-sm bg-blue-500 pointer-events-none" style= "background-color: ${backgroundColor};"></div>
        <p class="pointer-events-none">${board.name}</p>
        <div class="dropdown">
          <div tabindex="0" role="button" class="btn m-1">...</div>
          <ul
            tabindex="0"
            class="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li><a>Delete Board</a></li>
          </ul>
        </div>`;
}

//creating lists 
function createListAndCards(list, listCont) {
  listCont.innerHTML += `<div class="bg-[#ebecf0] max-w-80 p-4 rounded-xl h-fit" >
        <div class="flex gap-5">
          <input
            type="text"
            placeholder="Type here"
            class="input w-9/12 bg-[#ebecf0] focus:bg-white text-xl"
            value="${list.name}"
          />
          <div class="dropdown bg-[#ebecf0]">
            <div tabindex="0" role="button" class="btn m-1 bg-[#ebecf0]">
              ...
            </div>
            <ul
              tabindex="0"
              class="dropdown-content z-[1] menu p-2 rounded-box w-52 bg-[#ebecf0]"
            >
              <li class="delete-list-btn"><a>Delete List</a></li>
            </ul>
          </div>
        </div>
        <div class="card-conatiner flex flex-col gap-2">
          
        </div>
        <div
          class="flex min-h-10 mt-2 gap-2 items-center hover:bg-white pl-3 rounded-lg add-card"
        >
        <div  class="flex gap-3 pointer-events-none">
          <p>+</p>
          <p>Add a card</p>
        </div>
        
        <div class="hidden flex-wrap gap-3">
          <input
            type="text"
            placeholder="Enter a title for this card..."
            class="input w-11/12"
          />
          <div class="flex gap-3">
            <button class="btn btn-primary">Add card</button>
            <button class="btn btn-square" id="close-btn-card">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /></svg>
            </button>
          </div>
        </div>  
      </div>
`;
}

//attaching eventlistener for adding a list
async function createListEL(){
  const addListDiv = document.getElementById("add-list");
  const addListBtn = document.querySelector("#add-list>div:nth-child(1)");
  const addListModel = document.querySelector("#add-list>div:nth-child(2)");

  addListModel.addEventListener("click",(e)=>{
    if(e.target.tagName === 'BUTTON' && e.target.innerText === "Add list"){
      const listName = e.target.parentElement.previousElementSibling.value;
      if(listName === "") return;
      createList(listName,boardId);
      setTimeout(()=>{
        location.reload();
      },500)
    }
  })
  addListDiv.addEventListener("click",(e)=>{
    // console.log(e.target);
    if(e.target.tagName === "DIV" && addListBtn.style.display !== "none"){
      e.target.style.height = "8.5rem";
      addListBtn.style.display = "none";
      addListModel.style.display = "flex";
    }else if(e.target.id === "close-btn" || e.target.tagName === 'svg' || e.target.tagName === 'path'){
      e.preventDefault();
      addListDiv.style.height = "3rem";
      addListBtn.style.display = "flex";
      addListModel.style.display = "none";
    }
  })
}

//attaching eventlisteners for creating a card 
async function createCardEL(lists){
  const addCardDivs = document.querySelectorAll(".add-card");
  for (let index = 0; index < addCardDivs.length; index++) {
    const addCardDiv = addCardDivs[index];
    const addCardBtn = addCardDiv.children[0];
    const addCardModel = addCardDiv.children[1];
    const list = lists[index];

    addCardModel.addEventListener("click",(e)=>{
      if(e.target.tagName === 'BUTTON' && e.target.innerText === "Add card"){
        const cardName = e.target.parentElement.previousElementSibling.value;
        if(cardName === "") return;
        createCard(list.id,cardName);
        setTimeout(()=>{
          location.reload();
        },500)
      }
    })
    addCardDiv.addEventListener("click",(e)=>{
      // console.log(e.target);
      if(e.target.tagName === "DIV" && addCardBtn.style.display !== "none"){
        addCardBtn.style.display = "none";
        addCardModel.style.display = "flex";
        addCardDiv.style.backgroundColor = "#ebecf0";
      }else if(e.target.id === "close-btn-card" || e.target.tagName === 'svg' || e.target.tagName === 'path'){
        e.preventDefault();
        addCardBtn.style.display = "flex";
        addCardModel.style.display = "none";
      }
    })

  }
}

//attaching eventlisteners for archiving/deleting a list
function archiveListUI(lists){
  const deleteBtns = document.querySelectorAll(".delete-list-btn");
  for (let index = 0; index < deleteBtns.length; index++) {
    const deleteBtn = deleteBtns[index];
    const list = lists[index];
    deleteBtn.addEventListener("click",()=>{
      archiveListfun(list.id);
      setTimeout(()=>{
        location.reload();
      },500)
    })
  }
}

async function archiveListfun(listId){
  await archiveList(listId);
}
